<?php
require_once('simpletest/autorun.php');
require ('includes/bPasses.php');
require ('includes/bPass.php');


// read in file data and decode into JSON array
$filedata = file_get_contents('inputtest.json');
$filejson = json_decode($filedata);
bPasses::convertJSON($filejson);


class RouteTest extends UnitTestCase {
    function testReadJSON() {
        $this->assertEqual(count(bPasses::$passArray), 4);
    }
    function testRouteStart() {
        $this->assertEqual(bPasses::routeStart(), 2);
    }
}
?>