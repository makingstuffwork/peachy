<?php

class bPass{

    var $baggage='';
    var $seat = '';
    var $gate = '';
    var $from = '';
    var $to = '';
    var $type = '';
    var $typeInfo = '';

    public function populate($index,$jsonArrayItem,&$searchIndex){

        foreach($jsonArrayItem as $jsonKey=>$jsonValue){
            if(isset($this->$jsonKey)){
                $this->$jsonKey = $jsonValue;
            }
        }

        if(isset($jsonArrayItem->{$jsonArrayItem->type})){
            $this->typeInfo = $jsonArrayItem->{$jsonArrayItem->type};
        }

        // as we set the variables, keep the search index up to date - saves an additional step in
        // the future to index the data

        $searchIndex['to'][$this->to]=$index;
        $searchIndex['from'][$this->from]=$index;

    }

    public function getFriendly(){

        // just a demo of what can be done - we are automatically choosing from one of three sentence structures
        // and outputting the step details. 

        if(!$this->typeInfo==""){
            $this->typeInfo = $this->typeInfo." ";
        }

        // try not to have these, however we need to change whats been passed in for display
        $typefriendly = $this->type;

        if($this->type=="airportbus"){
            $typefriendly = "airport bus";
        }


        switch(rand(0,2)){
            case 0:
                $sentence = "Take ".$typefriendly." ".$this->typeInfo."from ".$this->from." to ".$this->to.". ";
            break;
            case 1:
                $sentence = "Take the ".$typefriendly." ".$this->typeInfo."from ".$this->from." to ".$this->to.". ";
            break;
            case 2:
                $sentence = "From ".$this->from.", take ".$typefriendly." ".$this->typeInfo."to ".$this->to.". ";
            break;
        }
        
        if(!$this->gate==""){
            $sentence .= "Gate ".$this->gate.", ";
        }

        if(!$this->seat==""){

            switch(rand(0,1)){
                case 0:
                $sentence .= "Seat ".$this->seat.". ";
                break;
                case 1:
                    $sentence .= "Sit in seat ".$this->seat.". ";
                break;
            }

        }else{
            $sentence .= "No seat assignment. ";
        }

        if(!$this->baggage==""){
            $sentence .= $this->baggage.". ";
        }

        return $sentence;

    }

}