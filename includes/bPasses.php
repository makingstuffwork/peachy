<?php

class bPasses {

    /**
    * @passArray    Array
    * @searchIndex  Array
    */

    public static $passArray = Array(); // array of pass objects
    public static $searchIndex = Array('to'=>array(),'from'=>array()); // contains lookup for FROM and TO locations
    public static $stepnumber = 0;

    public static function convertJSON(array $JSONArray) : bool{
        
        bPasses::$passArray = Array();

        foreach($JSONArray as $index=>$JSONticket){
            $abPass = new bPass;
            $abPass->populate($index,$JSONticket,bPasses::$searchIndex);
            bPasses::$passArray[$index] = $abPass;
        }

        return true;
    }

    public static function routeStart(){
        foreach(bPasses::$passArray as $index=>$abPass){
            if(!isset(bPasses::$searchIndex['to'][$abPass->from])){
                return intval($index);
            }
        }
    }

    public static function isEnd($abPass){
        if(!isset(bPasses::$searchIndex['from'][$abPass->to])){
            return True;
        }
        return False;
    }

    public static function returnOutput($currentPosition){
        bPasses::$stepnumber++;
        $currentstep = bPasses::$passArray[$currentPosition];
        echo bPasses::$stepnumber."   ";
        echo $currentstep->getFriendly()."\n";
        if(!bPasses::isEnd($currentstep)){
            bPasses::returnOutput(bPasses::$searchIndex['from'][$currentstep->to]);
        }else{
            bPasses::$stepnumber++;
            echo bPasses::$stepnumber."   "."You have arrived at your final destination.\n\n";
        }
    }

}