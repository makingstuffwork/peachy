<?php
require ('includes/bPasses.php');
require ('includes/bPass.php');

// ensure parameter has been passed
if(!isset($argv[1])){
    echo "Ensure you are passing the JSON file as a parameter. e.g. \nphp process.php input.json\n";
    exit;
}

// ensure file exists before processing
$filename = $argv[1];
if(!file_exists($filename)){
    echo "Cannot read file $filename\n";
    exit;
}

// read in file data and decode into JSON array
$filedata = file_get_contents($filename);
$filejson = json_decode($filedata);

// ensure parsed json has elements ( valid JSON and more than 1 element long ) 
if(!is_array($filejson)){
    echo "Cannot read JSON from file $filename\n";
    exit;
}

// convert JSON string array into bPass objects
bPasses::convertJSON($filejson);

// find start step 
$start = bPasses::routeStart();

// newline for layout on command line
echo "\n";

// output details for each step
bPasses::returnOutput($start);